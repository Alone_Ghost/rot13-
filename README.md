#Rot13
  A software **crypto** your **message**
  
#Algorithm
[Wikipedia](https://en.wikipedia.org/wiki/ROT13)
![Rot13](https://gitlab.com/Alone_Ghost/rot13-/blob/master/screenshot/rot13.jpg)
**############################################**

##if you like use this programing
##INSTALL PACKAGE

  *In RedHat base distributions (like Fedora):*
    `dnf install g++`

  *In Debian base distributions:*

    `sudo apt install g++`

  *In Arch base Distributions:*
    `sudo pacman -S base-devel`

###compile and run 

1. `g++ rot13.cpp`

2. `./a.out`
