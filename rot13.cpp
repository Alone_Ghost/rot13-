#include <iostream>
#include <string>

int main()
{

      char englishLetters[26] = {'a' , 'b' , 'c' , 'd' , 'e', 'f', 'g' , 'h' , 'i' , 'j', 'k' , 'l' , 'm' , 'n' , 'o' , 'p' , 'q' , 'r' , 's' , 't' , 'u' , 'v' , 'w' , 'x' , 'y' , 'z'  };

        std::cout << "Please Enter Your Message:" <<std::endl;
        std::string message;
        std::getline(std::cin , message);

        std::cout <<"\n";
        std::cout <<"Output:\n";

      for (int i = 0 ;  i < message.size() ; i++)
      {
        for (int j = 0 ; j < 26 ; j++)
        {
          if( message[i] == englishLetters[j])
          {
            if ( j >= 13 )
            {
              message[i] = englishLetters[j-13];
              std::cout<<message[i];
            }
            else if (j < 13)
            {
              message[i] = englishLetters[j+13];
                std::cout << message[i];
            }    
          break;
          }
          else if (j >= 25 )
          {
            std::cout<<" ";
          }
        }
      }
      std::cout<<std::endl;
}
